//
//  AppDelegate.swift
//  spotifybrowser
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit
import WebKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private lazy var context: AppContext = { [weak self] in
        let window: (() -> UIWindow) = { [weak self] in
            let w = self?.window ?? UIWindow(frame: UIScreen.main.bounds)
            self?.window = w
            return w
        }
        return AppContext(window: window)
    }()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Log.configure()
        Log.info("app version: \(application.version), build: \(application.build)")
        return context.services.compactMap { $0.application?(application, willFinishLaunchingWithOptions: launchOptions) }.reduce(true, { $0 || $1 })
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Log.debug("")
        return context.services.compactMap { $0.application?(application, didFinishLaunchingWithOptions: launchOptions) }.reduce(true, { $0 || $1 })
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        Log.debug("open url: \(url.absoluteString)")
        return context.services.compactMap { $0.application?(app, open: url, options: options) }.reduce(false, { $0 || $1 })
    }

    func applicationWillTerminate(_ application: UIApplication) {
        Log.debug("")
        context.services.forEach { $0.applicationWillTerminate?(application) }
    }
}
