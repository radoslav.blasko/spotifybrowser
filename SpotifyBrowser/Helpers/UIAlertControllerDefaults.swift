//
//  UIAlertControllerDefaults.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 10/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func defaultOkAlert(title: String = NSLocalizedString("Alert", comment: "Alert"),
                               message: String?,
                               handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default, handler: handler)
        alert.addAction(okAction)
        return alert
    }
}
