//
//  ReuseIdentifiable.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        return String(describing: type(of: self))
            .split { $0.isUppercase }
            .joined(separator: "-")
            .lowercased()
    }
}

extension UICollectionViewCell: ReuseIdentifiable { }
extension UITableViewCell: ReuseIdentifiable { }
