//
//  DictionaryQueryItemsHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 09/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension Dictionary where Key == String, Value == String {
    var queryItems: [URLQueryItem] {
        return compactMap({ key, value -> URLQueryItem in
            return URLQueryItem(name: key, value: value)
        })
    }
}
