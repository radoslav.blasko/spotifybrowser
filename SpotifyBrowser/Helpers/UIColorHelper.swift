//
//  UIColorHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 06/02/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIColor {
    var resolvedColorInverted: UIColor {
        let style = UITraitCollection.current.userInterfaceStyle
        let invertedStyle: UIUserInterfaceStyle

        switch style {
        case .dark:
            invertedStyle = .light
        case .light:
            invertedStyle = .dark
        default:
            invertedStyle = style
        }

        let trait = UITraitCollection(userInterfaceStyle: invertedStyle)
        return resolvedColor(with: trait)
    }

    func adjustingBrightnessBy(factor: CGFloat) -> UIColor {
        var hue = CGFloat()
        var sat = CGFloat()
        var brightness = CGFloat()
        var alpha = CGFloat()

        let success = getHue(&hue, saturation: &sat, brightness: &brightness, alpha: &alpha)
        guard success else { return self }

        return UIColor(hue: hue, saturation: sat, brightness: brightness * factor, alpha: alpha)
    }
}
