//
//  UIApplicationInfoHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIApplication {
    var version: String { return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "-" }
    var build: String { return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "-" }
}
