//
//  URLQueryHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 09/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension URL {
    var parameters: [String: String] {
        var parameters = [String: String]()

        let parametersSource = [query, fragment].compactMap { $0 }
        for s in parametersSource {
            let pairs = s.components(separatedBy: "&")
            pairs.forEach { pair in
                let p = pair.components(separatedBy: "=")
                parameters[p[0]] = p[1]
            }
        }

        return parameters
    }
}
