//
//  ViewStatePresenterHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 07/02/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

extension ViewStatePresenter.UserInfo {
    static var empty: Self {
        let title = NSLocalizedString("Nothing there yet", comment: "")
        let subtitle = NSLocalizedString("Come back later", comment: "")
        return .init(title: title, subtitle: subtitle)
    }
}
