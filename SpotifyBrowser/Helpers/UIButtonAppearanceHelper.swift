//
//  UIButtonAppearanceHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 07/10/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIAppearance where Self: UIButton {
    func setFont(_ font: UIFont) {
        let appearance = UILabel.appearance(whenContainedInInstancesOf: [Self.self])
        appearance.font = font
    }
}
