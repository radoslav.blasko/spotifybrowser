//
//  NibInstantiable.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

protocol NibInstantiable {
    static var nibName: String { get }
    static var nib: UINib { get }
}

extension NibInstantiable where Self: UIView {
    static var nibName: String { return String(String(describing: type(of: self)).split(separator: ".").first!) }
    static var nib: UINib { return UINib(nibName: nibName, bundle: nil) }
}

extension UICollectionViewCell: NibInstantiable { }
