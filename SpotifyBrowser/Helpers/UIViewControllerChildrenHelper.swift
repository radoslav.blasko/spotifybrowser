//
//  UIViewControllerChildrenHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 08/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIViewController {
    func add(child new: UIViewController) {
        addChild(new)
        new.view.frame = view.bounds
        view.addSubview(new.view)
        new.didMove(toParent: self)
    }

    func remove(child old: UIViewController) {
        old.willMove(toParent: nil)
        old.view.removeFromSuperview()
        old.removeFromParent()
    }

    func replace(child old: UIViewController?, with new: UIViewController?, animator: UIViewControllerAnimatedTransitioning = NativeTransitionAnimator.noAnimation) {
        guard isViewLoaded else {
            fatalError("View is not laoded yet: cannot do child view controllers transition!")
        }

        guard let from = old, let to = new else {
            fatalError("Not implemented yet!")
        }

        // If there is a modal view controller when swapping the childViewController, the modal VC is not dismissed,
        // but set its presentingViewController to be the new childViewController
        if from.presentedViewController != nil {
            from.dismiss(animated: true, completion: nil)
        }

        from.willMove(toParent: nil)
        addChild(to)

        let transitionContext = PrivateTransitionContext(fromViewController: from, toViewController: to, containerView: view) {
            from.view.removeFromSuperview()
            from.removeFromParent()
            to.didMove(toParent: self)
        }
        animator.animateTransition(using: transitionContext)
    }
}

private final class PrivateTransitionContext: NSObject {
    private let _containerView: UIView
    private let _completion: (() -> Void)?
    private let _viewControllers: [UITransitionContextViewControllerKey: UIViewController]

    init(fromViewController: UIViewController, toViewController: UIViewController, containerView: UIView? = nil, completion: (() -> Void)?) {
        guard let containerView = containerView ?? fromViewController.view.superview else {
            fatalError("Missing container view!")
        }
        _viewControllers = [.from: fromViewController,
                            .to: toViewController]
        _completion = completion
        _containerView = containerView
        super.init()
    }
}

extension PrivateTransitionContext: UIViewControllerContextTransitioning {
    var containerView: UIView { return _containerView }
    var isAnimated: Bool { return true }
    var isInteractive: Bool { return false }
    var transitionWasCancelled: Bool { return false }
    var presentationStyle: UIModalPresentationStyle { return .custom }
    var targetTransform: CGAffineTransform { return .identity }

    func updateInteractiveTransition(_ percentComplete: CGFloat) {}
    func finishInteractiveTransition() {}
    func cancelInteractiveTransition() {}
    func pauseInteractiveTransition() {}

    func completeTransition(_ didComplete: Bool) {
        _completion?()
    }

    func viewController(forKey key: UITransitionContextViewControllerKey) -> UIViewController? {
        return _viewControllers[key]
    }

    func view(forKey key: UITransitionContextViewKey) -> UIView? {
        return _viewControllers[key.convert()]?.view
    }

    func initialFrame(for vc: UIViewController) -> CGRect {
        return _containerView.bounds
    }

    func finalFrame(for vc: UIViewController) -> CGRect {
        return _containerView.bounds
    }
}

extension UITransitionContextViewKey {
    func convert() -> UITransitionContextViewControllerKey {
        return self == .from ? .from : .to
    }
}

final class NativeTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    enum Transition {
        case flipFromLeft
        case flipFromRight
        case flipFromTop
        case flipFromBottom

        case curlUp
        case curlDown

        case crossDissolve

        func toAnimationOption() -> UIView.AnimationOptions {
            switch self {
                case .flipFromRight: return .transitionFlipFromRight
                case .flipFromLeft: return .transitionFlipFromLeft
                case .flipFromTop: return .transitionFlipFromTop
                case .flipFromBottom: return .transitionFlipFromBottom

                case .curlUp: return .transitionCurlUp
                case .curlDown: return .transitionCurlDown

                case .crossDissolve: return .transitionCrossDissolve
            }
        }
    }

    static var noAnimation: NativeTransitionAnimator { .init(transition: .crossDissolve, duration: 0) }

    private let transition: Transition
    private let duration: TimeInterval

    init(transition: Transition, duration: TimeInterval = 0.25) {
        self.transition = transition
        self.duration = duration
        super.init()
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to) else {
                return
        }

        if !transitionContext.containerView.subviews.contains(toViewController.view) {
            transitionContext.containerView.addSubview(toViewController.view)
        }

        let frame = transitionContext.containerView.bounds
        toViewController.view.frame = frame

        let duration = transitionDuration(using: transitionContext)
        UIView.transition(from: fromViewController.view,
                          to: toViewController.view,
                          duration: duration,
                          options: [transition.toAnimationOption(), .curveEaseInOut]) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

class ModalTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    enum Transition {
        case presentation
        case dismissal
    }

    let transition: Transition

    init(transition: Transition) {
        self.transition = transition
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        fatalError("Override!")
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        fatalError("Override!")
    }
}

final class CoverAnimator: ModalTransitionAnimator {
    override func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }

    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to) else {
                return
        }

        var endFrame = UIScreen.main.bounds
        var startFrame = endFrame
        var viewController = toViewController

        switch transition {
        case .presentation:
            transitionContext.containerView.addSubview(toViewController.view)
            startFrame.origin = CGPoint(x: endFrame.origin.x, y: startFrame.height)
        case .dismissal:
            if !transitionContext.containerView.subviews.contains(toViewController.view) {
                transitionContext.containerView.addSubview(toViewController.view)
            }
            transitionContext.containerView.sendSubviewToBack(toViewController.view)
            toViewController.view.frame = startFrame
            viewController = fromViewController
            endFrame.origin = CGPoint(x: endFrame.origin.x, y: endFrame.height)
        }

        viewController.view.frame = startFrame

        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       delay: 0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 6.0,
                       options: .curveEaseIn,
                       animations: {
            viewController.view.frame = endFrame
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
