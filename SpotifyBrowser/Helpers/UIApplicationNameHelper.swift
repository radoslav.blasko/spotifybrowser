//
//  UIApplicationNameHelper.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 10/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIApplication {
    static var name: String {
        return Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
    }
}
