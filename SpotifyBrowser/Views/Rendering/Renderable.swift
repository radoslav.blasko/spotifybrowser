//
//  Renderable.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

protocol Renderable {
    associatedtype View: UIView
    func render(in view: View)
}
