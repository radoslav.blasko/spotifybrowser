//
//  AnimatedButton.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 07/02/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

class AnimatedButton: UIButton {

    enum TouchEvent {
        case touchDown
        case touchUp
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        adjustsImageWhenHighlighted = false
        addTarget(self, action: #selector(touchedUp(sender:)), for: [.touchUpInside,.touchUpOutside, .touchCancel])
        addTarget(self, action: #selector(touchedDown(sender:)), for: .touchDown)
    }

    @objc private func touchedDown(sender: UIButton) {
        transformButtonSizeAnimated(transform: transform(for: .touchDown), duration: transformDuration(for: .touchDown), event: .touchDown)
    }

    @objc private func touchedUp(sender: UIButton) {
        transformButtonSizeAnimated(transform: transform(for: .touchUp), duration: transformDuration(for: .touchUp), event: .touchUp)
    }

    func transformDuration(for event: TouchEvent) -> TimeInterval {
        switch event {
        case .touchDown: return 0.1
        case .touchUp: return 0.15
        }
    }

    func transform(for event: TouchEvent) -> CGAffineTransform {
        switch event {
        case .touchDown: return CGAffineTransform(scaleX: 0.92, y: 0.92)
        case .touchUp: return CGAffineTransform.identity
        }
    }

    func transformButtonSizeAnimated(transform: CGAffineTransform, duration: TimeInterval, event: TouchEvent) {
        UIView.animate(withDuration: duration) { self.transform = transform }
    }

}
