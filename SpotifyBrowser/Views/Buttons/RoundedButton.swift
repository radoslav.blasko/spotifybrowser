//
//  RoundedButton.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

class RoundedButton: AnimatedButton {
    @objc dynamic var radius: CGFloat = 5 {
        didSet {
            applyRadius(radius)
        }
    }

    convenience init(radius: CGFloat) {
        self.init(type: .custom)
        self.radius = radius
        _init()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        assert(buttonType == .custom, "\(self) must be initialized with buttonType == .custom!")
        _init()
    }

    private func _init() {
        heightAnchor.constraint(greaterThanOrEqualToConstant: 44).isActive = true
        applyRadius(radius)
    }

    private func applyRadius(_ r: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
}
