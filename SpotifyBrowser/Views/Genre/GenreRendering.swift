//
//  GenreRendering.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 06/02/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

extension SpotifyAPI.v1.Response.Genre: Renderable {
    func render(in cell: GenreCollectionViewCell) {
        cell.titleLabel.text = self

        let colors = UIColor.palette.randomColors
        let nums = self.compactMap { $0.asciiValue }.map { Int($0) }
        let index = (nums.reduce(0, +) / nums.count) % colors.count
        cell.contentView.backgroundColor = colors[index]
    }
}
