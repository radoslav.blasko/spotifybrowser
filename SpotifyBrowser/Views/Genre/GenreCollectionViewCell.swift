//
//  GenreCollectionViewCell.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 06/02/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

class GenreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 6
        titleLabel.textColor = UIColor.palette.buttonTitle
        titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
    }
}
