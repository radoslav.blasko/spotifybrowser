//
//  UIViewControllerStatePresenterHelper.swift
//  UIHelpers
//
//  Created by Radoslav Blasko on 24/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

protocol ViewSatePresenting {
    var statePresenter: ViewStatePresenter { get }
}

private struct AssociatedKeys {
    static var statePresenter = "com.viewSatePresenting.statePresenter.key"
}

extension ViewSatePresenting where Self: UIViewController {
    var statePresenter: ViewStatePresenter {
        guard isViewLoaded else { fatalError("Trying to acccess ViewStatePresenter before the view is loaded!") }

        if let sp = objc_getAssociatedObject(self, AssociatedKeys.statePresenter) as? ViewStatePresenter {
            return sp
        } else {
            let statePresenter = ViewStatePresenter(rootView: view)
            objc_setAssociatedObject(self, AssociatedKeys.statePresenter, statePresenter, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return statePresenter
        }
    }
}
