//
//  CategoryCollectionViewCell.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var overlayView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 6
        nameLabel.textColor = .white
        nameLabel.shadowOffset = .init(width: 0, height: 0)
        nameLabel.shadowColor = UIColor.palette.background.resolvedColorInverted
        overlayView.backgroundColor = UIColor.palette.background
        overlayView.alpha = 0
        categoryImageView.backgroundColor = UIColor.palette.secondaryBackground.adjustingBrightnessBy(factor: 0.7)
    }

    override var isHighlighted: Bool {
        didSet {
            overlayView.alpha = self.isHighlighted ? 0.1 : 0
        }
    }

    override var isSelected: Bool {
        didSet {
            overlayView.alpha = self.isSelected ? 0.1 : 0
        }
    }
}
