//
//  UserError.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import Foundation

struct UserError {
    var title: String = NSLocalizedString("Error", comment: "")
    var description: String
}

extension UserError {
    init(_ error: Error) {
        self = .init(description: error.localizedDescription)
    }
}

extension UserError {
    var userInfo: ViewStatePresenter.UserInfo {
        return ViewStatePresenter.UserInfo(title: title, subtitle: description, img: nil)
    }
}
