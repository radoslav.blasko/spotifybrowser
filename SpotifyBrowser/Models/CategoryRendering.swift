//
//  CategoryRendering.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import Foundation
import Kingfisher

extension SpotifyAPI.v1.Response.Category: Renderable {
    func render(in cell: CategoryCollectionViewCell) {
        cell.nameLabel.text = name

        if let urlString = icons.first?.url, let url = URL(string: urlString) {
            cell.categoryImageView.kf.setImage(with: url)
        } else {
            cell.categoryImageView.kf.cancelDownloadTask()
        }
    }
}
