//
//  LogFormatter.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CocoaLumberjackSwift

final class LogFormatter: NSObject, DDLogFormatter {
    let isUsingEmoji: Bool
    private let dateFormatter: DateFormatter

    struct Colors {
        static var reset = "\u{001b}[0m"
        static var escape = "\u{001b}[38;5;"
        static var verbose = "251m"
        static var debug = "35m"
        static var info = "38m"
        static var warning = "178m"
        static var error = "197m"
    }

    init(isUsingEmoji: Bool) {
        self.isUsingEmoji = isUsingEmoji
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-mm-dd HH:mm:ss:sss"
    }

    func format(message logMessage: DDLogMessage) -> String? {
        let level: String
        let color: String

        switch logMessage.flag {
        case .debug:
            color = isUsingEmoji ? "💚" : Colors.debug
            level = "DEBUG"
        case .error:
            color = isUsingEmoji ? "❤️" : Colors.error
            level = "ERROR"
        case .info:
            color = isUsingEmoji ? "💙" : Colors.info
            level = "INFO"
        case .verbose:
            color = isUsingEmoji ? "💜" : Colors.verbose
            level = "VERBOSE"
        case .warning:
            color = isUsingEmoji ? "💛" : Colors.warning
            level = "WARNING"
        default:
            color = Colors.info
            level = ""
        }

        let coloredLevel = isUsingEmoji ? color + " " + level : Colors.escape + color + level + Colors.reset
        let functionInfo = "\(logMessage.function ?? "-"):\(String(logMessage.line))"
        let message = logMessage.message.isEmpty ? "" :  " - \(logMessage.message)"

        return "\(dateFormatter.string(from: logMessage.timestamp)) \(coloredLevel) \(logMessage.fileName).\(functionInfo)" + message
    }
}
