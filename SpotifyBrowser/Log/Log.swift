//
//  Log.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CocoaLumberjackSwift

final class Log {
    static func configure() {
        let fileLogLevel: DDLogLevel

        switch Environment.current {
        case .develop:
            fileLogLevel = .verbose

            let console = DDTTYLogger()
            console.logFormatter = LogFormatter(isUsingEmoji: true)
            DDLog.add(console, with: .debug)
        case .staging, .production:
            fileLogLevel = .warning
        }

        let fileLogger: DDFileLogger

        if DeviceInfo.current.isSimulator {
            let fileManager = DDLogFileManagerDefault(logsDirectory: "/tmp/")
            fileLogger = .init(logFileManager: fileManager)
        } else {
            fileLogger = .init()
        }

        fileLogger.logFormatter = LogFormatter(isUsingEmoji: false)
        fileLogger.maximumFileSize = 1024 * 1024 * 3
        fileLogger.rollingFrequency = 60 * 60 * 24
        DDLog.add(fileLogger, with: fileLogLevel)

        Log.debug("Configure logging")
        Log.debug("log file: \(fileLogger.logFileManager.sortedLogFilePaths.first ?? "")")
    }

    /// log something generally unimportant (lowest priority)
    class func verbose(_ message: @autoclosure () -> String, _ file: StaticString = #file,
                       _ function: StaticString = #function, line: UInt = #line, context: Any? = nil) {
        DDLogVerbose(message(), file: file, function: function, line: line)
    }

    /// log something which help during debugging (low priority)
    class func debug(_ message: @autoclosure () -> String, _ file: StaticString = #file,
                     _ function: StaticString = #function, line: UInt = #line, context: Any? = nil) {
        DDLogDebug(message(), file: file, function: function, line: line)
    }

    /// log something which you are really interested but which is not an issue or error (normal priority)
    class func info(_ message: @autoclosure () -> String, _ file: StaticString = #file,
                    _ function: StaticString = #function, line: UInt = #line, context: Any? = nil) {
        DDLogInfo(message(), file: file, function: function, line: line)
    }

    /// log something which may cause big trouble soon (high priority)
    class func warning(_ message: @autoclosure () -> String, _ file: StaticString = #file,
                       _ function: StaticString = #function, line: UInt = #line, context: Any? = nil) {
        DDLogWarn(message(), file: file, function: function, line: line)
    }

    /// log something which will keep you awake at night (highest priority)
    class func error(_ message: @autoclosure () -> String, _ file: StaticString = #file,
                     _ function: StaticString = #function, line: UInt = #line, context: Any? = nil) {
        DDLogError(message(), file: file, function: function, line: line)
    }
}
