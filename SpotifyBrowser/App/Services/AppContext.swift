//
//  AppContext.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

class AppContext {
    let authService: SpotifyAuthService
    let appearanceService: AppearanceService
    let appNavigationService: AppNavigationService
    let apiClient = SpotifyAPI.client
    let storage = StorageService()
    let services: [UIApplicationDelegate]

    init(window: @escaping (() -> UIWindow)) {
        appearanceService = AppearanceService()
        authService = SpotifyAuthService(clientId: Config.SpotifyAPI.clientId)
        appNavigationService = AppNavigationService(withWindow: window, apiClient: apiClient, authService: authService, storage: storage)
        services = [appearanceService, authService, appNavigationService, storage]

        let accessToken: (() -> SpotifyAPI.OAuth2.Token?) = { self.storage.accessToken }
        apiClient.sessionManager.adapter = SpotifyAPI.RequestAdapter(accessToken: accessToken)
        apiClient.sessionManager.retrier = SpotifyAPI.RequestRetrier(accountsClient: SpotifyAPI.accounts,
                                                                     clientId: Config.SpotifyAPI.clientId,
                                                                     clientSecret: Config.SpotifyAPI.clientSecret,
                                                                     storage: storage)
    }
}
