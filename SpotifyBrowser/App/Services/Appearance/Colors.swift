//
//  Colors.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 31/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

extension UIColor {
    enum palette {
        static var buttonFill: UIColor { .init(light: #colorLiteral(red: 0.108508639, green: 0.8441061378, blue: 0.3761509061, alpha: 1)) }
        static var buttonTitle: UIColor { .init(light: #colorLiteral(red: 0.980805099, green: 0.9737146497, blue: 0.9862335324, alpha: 1)) }
        
        static var tint: UIColor { buttonFill }

        static var background: UIColor { .init(light: #colorLiteral(red: 0.980805099, green: 0.9737146497, blue: 0.9862335324, alpha: 1), dark: #colorLiteral(red: 0.04257143289, green: 0.05049043894, blue: 0.04637859017, alpha: 1)) }
        static var secondaryBackground: UIColor { .init(light: #colorLiteral(red: 0.980805099, green: 0.9737146497, blue: 0.9862335324, alpha: 1).adjustingBrightnessBy(factor: 1.2),
                                                        dark: #colorLiteral(red: 0.04257143289, green: 0.05049043894, blue: 0.04637859017, alpha: 1).adjustingBrightnessBy(factor: 1.2)) }
        static var caption: UIColor { .init(light: #colorLiteral(red: 0.04257143289, green: 0.05049043894, blue: 0.04637859017, alpha: 1), dark: #colorLiteral(red: 0.980805099, green: 0.9737146497, blue: 0.9862335324, alpha: 1)) }

        static var randomColors: [UIColor] {
            return [#colorLiteral(red: 0.3725490196, green: 0.7882352941, blue: 0.9725490196, alpha: 1), #colorLiteral(red: 0.9960784314, green: 0.7960784314, blue: 0.1803921569, alpha: 1), #colorLiteral(red: 0.9882352941, green: 0.1921568627, blue: 0.3450980392, alpha: 1), #colorLiteral(red: 0.07843137255, green: 0.4941176471, blue: 0.9843137255, alpha: 1), #colorLiteral(red: 0.3254901961, green: 0.8431372549, blue: 0.4117647059, alpha: 1), #colorLiteral(red: 0.9882352941, green: 0.2392156863, blue: 0.2235294118, alpha: 1)]
        }
    }
}

extension UIColor {
    convenience init(light: UIColor, dark: UIColor? = nil, unspecified: UIColor? = nil) {
        self.init { traitCollection -> UIColor in
            switch traitCollection.userInterfaceStyle {
            case .light:
                return light
            case .dark:
                return dark ?? light
            case .unspecified:
                return unspecified ?? light
            @unknown default:
                return light
            }
        }
    }
}
