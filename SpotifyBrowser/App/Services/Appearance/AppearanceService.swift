//
//  AppearanceService.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

protocol AppearanceStyle {
    static func applyStyle()
}

class AppearanceService: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        RoundedButton.applyStyle()
        UIWindow.applyStyle()
        UITabBar.applyStyle()

        return true
    }
}

extension RoundedButton: AppearanceStyle {
    static func applyStyle() {
        let appearance = RoundedButton.appearance()
        appearance.radius = 22
        appearance.setTitleColor(UIColor.palette.buttonTitle, for: .normal)
        appearance.contentEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
        appearance.setBackgroundColor(UIColor.palette.buttonFill, for: .normal)
        appearance.setFont(UIFont.systemFont(ofSize: 16, weight: .semibold))
    }
}

extension UIWindow: AppearanceStyle {
    static func applyStyle() {
        let appearance = UIWindow.appearance()
        appearance.tintColor = UIColor.systemGreen
        appearance.backgroundColor = UIColor.palette.background
    }
}

extension UITabBar: AppearanceStyle {
    static func applyStyle() {
        let appearance = UITabBar.appearance()
        appearance.tintColor = UIColor.palette.tint
        appearance.unselectedItemTintColor = UIColor.palette.caption
    }
}
