//
//  RootCoordinator.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

final class RootCoordinator {
    typealias Coordinator = Any

    private(set) var rootViewController = UIViewController()

    private var childCoordinators = [Coordinator]()
    private let apiClient: HTTPClient
    private let authService: SpotifyAuthService
    private let storage: StorageService

    init(flow: AppFlow, authService: SpotifyAuthService, apiClient: HTTPClient, storage: StorageService) {
        self.authService = authService
        self.storage = storage
        self.apiClient = apiClient

        present(flow: flow)
    }

    private func createCoordinator(for flow: AppFlow) -> (Any, UIViewController) {
        switch flow {
        case .login:
            let coordinator = SpotifyLoginCoordinator(authService: authService, apiClient: SpotifyAPI.accounts, scopes: .readOnly)
            coordinator.onComplete = { [weak self] result in
                guard let self = self else { return }

                self.storage.accessToken = result.accessToken
                self.storage.refreshToken = result.refreshToken

                self.present(flow: .home(flow: .browse))
            }

            return (coordinator, coordinator.rootViewController)
        case .home(let homeFlow):
            let coordinator = HomeCoordinator(apiClient: apiClient, flow: homeFlow)
            return (coordinator, coordinator.rootViewController)
        }
    }

    private func present(flow: AppFlow) {
        let (coordinator, viewController) = createCoordinator(for: flow)

        if childCoordinators.isEmpty {
            rootViewController.add(child: viewController)
        } else {
            rootViewController.replace(child: rootViewController.children.first,
                                       with: viewController,
                                       animator: CoverAnimator(transition: .dismissal))
            childCoordinators.removeLast()
        }

        childCoordinators.append(coordinator)
    }
}
