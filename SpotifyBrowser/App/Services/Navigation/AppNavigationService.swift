//
//  AppNavigationService.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 10/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

class AppNavigationService: NSObject, UIApplicationDelegate {
    private let window: (() -> UIWindow)
    private let rootCoordinator: RootCoordinator

    init(withWindow window: @escaping (() -> UIWindow), apiClient: HTTPClient, authService: SpotifyAuthService, storage: StorageService) {
        self.window = window

        let startupFlow: AppFlow = storage.accessToken == nil ? .login : .home(flow: .browse)
        rootCoordinator = RootCoordinator(flow: startupFlow, authService: authService, apiClient: apiClient, storage: storage)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        window().rootViewController = rootCoordinator.rootViewController
        window().makeKeyAndVisible()

        return true
    }
}
