//
//  AppFlow.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

enum AppFlow {
    case login
    case home(flow: HomeFlow)

    enum HomeFlow {
        case browse
        case library
    }
}
