//
//  StorageService.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

final class StorageService: NSObject, UIApplicationDelegate {
    @Stored(key: "access_token", defaultValue: nil)
    var accessToken: SpotifyAPI.OAuth2.Token?

    @Stored(key: "refresh_token", defaultValue: nil)
    var refreshToken: SpotifyAPI.OAuth2.Token?
}
