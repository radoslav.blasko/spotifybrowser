//
//  SpotifyAuthServiceErrorCodes.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAuthService {
    enum Error: Swift.Error {
        case accessDenied
        case unknown

        init(response: String?) {
            switch response {
            case "access_denied":
                self = .accessDenied
            default:
                self = .unknown
            }
        }
    }
}
