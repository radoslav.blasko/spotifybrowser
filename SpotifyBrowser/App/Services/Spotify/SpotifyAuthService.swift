//
//  SpotifyAuthService.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

final class SpotifyAuthService: NSObject, UIApplicationDelegate {
    typealias AuthCallback = (Result<Response, SpotifyAuthService.Error>) -> Void

    private let redirectUri = AppURL(scheme: .base, type: .oauthCallback).url
    private let clientId: String
    private let application: UIApplication
    private var authCallback: AuthCallback?

    init(clientId: String, application: UIApplication = UIApplication.shared) {
        self.clientId = clientId
        self.application = application
        super.init()
    }

    func authorize(scopes: [SpotifyAPI.OAuth2.Scope] = [], showDialog: Bool = false, callback: @escaping AuthCallback) {
        authCallback = callback

        let scope = scopes.map { $0.rawValue }.joined(separator: " ")

        let key = SpotifyAPI.OAuth2.AuthRequestKey.self
        let authParams = [key.scope.rawValue: scope,
                          key.redirectUri.rawValue: redirectUri.absoluteString,
                          key.clientId.rawValue: clientId,
                          key.responseType.rawValue: "code",
                          key.showDialog.rawValue: showDialog ? "true" : "false"]

        var authURLComponents = URLComponents(string: Config.SpotifyAPI.authURL)
        authURLComponents?.queryItems = authParams.queryItems

        guard let url = authURLComponents?.url else {
            fatalError("Could not create authorize URL from components: \(String(describing: authURLComponents))")
        }
        application.open(url, options: [:], completionHandler: nil)
    }

    // MARK: - UIApplicationDelegate
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        defer { authCallback = nil }

        guard authCallback != nil,
            let appURL = AppURL(url: url), appURL.scheme == .base, appURL.type == .oauthCallback,
            let params = appURL.parameters else { return true }

        if let code = params[SpotifyAPI.OAuth2.CallbackKey.code.rawValue] {
            let response = Response(code: code,
                                    state: params[SpotifyAPI.OAuth2.CallbackKey.state.rawValue],
                                    redirectUri: redirectUri.absoluteString)
            authCallback?(.success(response))
        } else {
            let error = SpotifyAuthService.Error(response: params[SpotifyAPI.OAuth2.CallbackKey.error.rawValue])
            authCallback?(.failure(error))
        }

        return true
    }
}

extension SpotifyAuthService {
    struct Response {
        let code: String
        let state: String?
        let redirectUri: String
    }
}
