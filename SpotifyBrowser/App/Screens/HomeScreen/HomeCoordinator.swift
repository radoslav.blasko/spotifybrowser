//
//  HomeCoordinator.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

final class HomeCoordinator {
    lazy var rootViewController: UINavigationController = { return createHomeViewController() }()

    private let tabBarViewController = UITabBarController(nibName: nil, bundle: nil)
    private let apiClient: HTTPClient

    init(apiClient: HTTPClient, flow: AppFlow.HomeFlow) {
        self.apiClient = apiClient
        selectFlow(flow: flow)
    }

    private func createHomeViewController() -> UINavigationController {
        tabBarViewController.viewControllers = [BrowseViewController(apiClient: apiClient), LibraryViewController()]
        return UINavigationController(rootViewController: tabBarViewController)
    }

    private func selectFlow(flow: AppFlow.HomeFlow) {
        let index: Int

        switch flow {
        case .browse:
            index = 0
        case .library:
            index = 1
        }

        tabBarViewController.selectedIndex = index
    }
}
