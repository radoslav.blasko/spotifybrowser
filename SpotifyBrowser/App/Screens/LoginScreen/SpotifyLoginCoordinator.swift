//
//  SpotifyLoginCoordinator.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

final class SpotifyLoginCoordinator {
    struct Result {
        let accessToken: SpotifyAPI.OAuth2.Token
        let refreshToken: SpotifyAPI.OAuth2.Token
        let scopes: SpotifyAPI.OAuth2.Scopes
    }

    lazy var rootViewController: UIViewController = { return createLoginViewController() }()
    var onComplete: ((Result) -> Void)?

    private let scopes: [SpotifyAPI.OAuth2.Scope]
    private let authService: SpotifyAuthService
    private let apiClient: HTTPClient

    init(authService: SpotifyAuthService, apiClient: HTTPClient, scopes: SpotifyAPI.OAuth2.Scopes) {
        self.authService = authService
        self.apiClient = apiClient
        self.scopes = scopes
    }

    private func createLoginViewController() -> SpotifyLoginViewController {
        return SpotifyLoginViewController { [weak self] completion in self?.authorize(completion: completion)}
    }

    private func authorize(completion: @escaping SpotifyLoginViewController.LoginCompletion) {
        authService.authorize(scopes: scopes, showDialog: false) { [weak self] result in
            switch result {
            case .success(let response):
                self?.getAccessToken(authResponse: response, completion: completion)
            case .failure(let error):
                completion(error)
            }
        }
    }

    private func getAccessToken(authResponse: SpotifyAuthService.Response, completion: @escaping SpotifyLoginViewController.LoginCompletion) {
        let grantType: SpotifyAPI.v1.Endpoint.Token.GrantType = .authorizationCode(code: authResponse.code, redirectUri: authResponse.redirectUri)
        let endpoint = SpotifyAPI.v1.Endpoint.Token.get(clientId: Config.SpotifyAPI.clientId,
                                                        clientSecret: Config.SpotifyAPI.clientSecret,
                                                        grantType: grantType)
        apiClient.request(endpoint) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(error)
            case .success(let response):
                completion(nil)

                let accessToken = SpotifyAPI.OAuth2.Token(token: response.accessToken, expiresIn: response.expiresIn)
                let refreshToken = SpotifyAPI.OAuth2.Token(token: response.refreshToken!, expires: nil)
                let result = Result(accessToken: accessToken, refreshToken: refreshToken, scopes: self.scopes)
                self.onComplete?(result)
            }
        }
    }
}
