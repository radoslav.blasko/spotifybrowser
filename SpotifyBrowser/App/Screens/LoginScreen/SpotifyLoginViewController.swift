//
//  LoginViewController.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 10/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import UIKit

class SpotifyLoginViewController: UIViewController, ViewSatePresenting {
    typealias LoginCompletion = (Error?) -> Void
    typealias LoginButtonHandler = (_ onLoginCompleted: @escaping LoginCompletion) -> Void
    var onLoginButtonTapped: LoginButtonHandler?

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!

    init(onLoginButtonTapped: @escaping LoginButtonHandler) {
        super.init(nibName: "SpotifyLoginViewController", bundle: nil)
        self.onLoginButtonTapped = onLoginButtonTapped
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError() }

    override func viewDidLoad() {
        super.viewDidLoad()

        descriptionLabel.text = NSLocalizedString("To use \(UIApplication.name), you need to sign in to your Spotify account.",
                                                  comment: "Sign in with Spotify description")

        let loginButtonTitle = NSLocalizedString("Sign in with Spotify", comment: "Sign in with Spotify")
        loginButton.setTitle(loginButtonTitle, for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)

        view.backgroundColor = UIColor.palette.background
    }

    @objc private func loginButtonTapped() {
        guard let onLoginButtonTapped = onLoginButtonTapped else { return }
        onLoginButtonTapped { [weak self] error in
            if let error = error {
                self?.presentError(error)
            } else {
                
            }
        }
    }

    private func presentError(_ error: Error) {
        let title = NSLocalizedString("Alert", comment: "Alert")
        let message = NSLocalizedString("Hmm, seems like spotify wont let you in.\n Try your luck again!",
                                        comment: "Hmm, seems like spotify wont let you in.")
        let userInfo = ViewStatePresenter.UserInfo(title: title, subtitle: message, img: nil, action: nil)
        statePresenter.present(state: .error, userInfo: userInfo)
    }
}
