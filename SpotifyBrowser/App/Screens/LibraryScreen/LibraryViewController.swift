//
//  LibraryViewController.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 31/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController {

    init() {
        super.init(nibName: "LibraryViewController", bundle: nil)

        let title = NSLocalizedString("Library", comment: "Library")
        tabBarItem = UITabBarItem(title: title, image: UIImage(systemName: "headphones"), selectedImage: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.palette.background
    }
}
