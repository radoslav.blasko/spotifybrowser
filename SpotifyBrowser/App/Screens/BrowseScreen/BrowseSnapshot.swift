//
//  BrowseSnapshot.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 12/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import UIKit

enum Browse { }

extension Browse {
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Item>

    enum Section: String {
        case categories
        case genres
    }

    enum Item {
        case category(item: SpotifyAPI.v1.Response.Category)
        case genre(item: SpotifyAPI.v1.Response.Genre)
    }
}

extension Browse.Item: Hashable { }
