//
//  BrowseViewController.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 31/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit

class BrowseViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!

    private var dataSource: UICollectionViewDiffableDataSource<Browse.Section, Browse.Item>! = nil
    private var statePresenter: ViewStatePresenter!
    private let viewModel: BrowseViewModel

    init(apiClient: HTTPClient) {
        viewModel = BrowseViewModel(apiClient: apiClient)
        
        super.init(nibName: "BrowseViewController", bundle: nil)

        let title = NSLocalizedString("Browse", comment: "Browse")
        self.tabBarItem = UITabBarItem(title: title, image: UIImage(systemName: "hifispeaker"), selectedImage: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func loadView() {
        super.loadView()

        statePresenter = ViewStatePresenter(rootView: view)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.palette.background

        configureCollectionView()
        configureDataSource()
        bind()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.load()
    }

    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { collectionView, indexPath, item in
            switch item {
            case .category(let category):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.reuseIdentifier, for: indexPath) as! CategoryCollectionViewCell
                category.render(in: cell)

                return cell
            case .genre(let item):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GenreCollectionViewCell.reuseIdentifier, for: indexPath) as! GenreCollectionViewCell
                item.render(in: cell)

                return cell
            }
        })
    }

    private func configureCollectionView() {
        collectionView.register(CategoryCollectionViewCell.nib, forCellWithReuseIdentifier: CategoryCollectionViewCell.reuseIdentifier)
        collectionView.register(GenreCollectionViewCell.nib, forCellWithReuseIdentifier: GenreCollectionViewCell.reuseIdentifier)
        collectionView.collectionViewLayout = BrowseLayout.create()
        collectionView.backgroundColor = nil
    }

    private func bind() {
        viewModel.snapshot = { [weak self] snapshot, animated in
            guard let self = self else { return }
            self.dataSource.apply(snapshot, animatingDifferences: animated)
        }

        viewModel.viewState = { [weak self] state, userInfo in
            guard let self = self else { return }
            self.statePresenter.present(state: state, userInfo: userInfo)
        }
    }
}
