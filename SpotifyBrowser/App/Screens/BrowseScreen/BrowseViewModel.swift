//
//  BrowseViewModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 31/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import UIKit
import Alamofire

final class BrowseViewModel {
    var snapshot: ((Browse.Snapshot, Bool) -> Void)?
    var viewState: ((ViewStatePresenter.State?, ViewStatePresenter.UserInfo?) -> Void)? {
        didSet {
            guard !isLoaded else { return }
            viewState?(.empty, .empty)
        }
    }

    private var isLoaded = false
    private let apiClient: HTTPClient

    init(apiClient: HTTPClient) {
        self.apiClient = apiClient
    }

    func load() {
        if isLoaded {
            return
        }

        viewState?(.activity, nil)

        var snapshot = Browse.Snapshot()
        snapshot.appendSections([.categories, .genres])

        let progress: ((Alamofire.Result<[Browse.Item]>) -> Void) = { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                Log.error("could not load categories: \(error.localizedDescription)")

                self.isLoaded = false

                var info = UserError(error).userInfo
                info.action = .init(title: "Try again") { [weak self] in self?.load() }

                self.viewState?(.error, info)
            case .success(let items):
                guard let first = items.first else { return }

                let section: Browse.Section

                switch first {
                case .category:
                    section = .categories
                case .genre:
                    section = .genres
                }

                snapshot.appendItems(items, toSection: section)

                self.isLoaded = true
                self.snapshot?(snapshot, true)
                self.viewState?(nil, nil)
            }
        }

        let categories = SpotifyAPI.v1.Endpoint.Browse.categories(paging: SpotifyAPI.PagingParam(limit: 50))
        apiClient.request(categories) { result in
            let result = result.flatMap { $0.categories.items.map { Browse.Item.category(item: $0) } }
            progress(result)
        }

        let genres = SpotifyAPI.v1.Endpoint.Browse.recommendationGenres(paging: SpotifyAPI.PagingParam(limit: 50))
        apiClient.request(genres) { result in
            let result = result.flatMap { $0.genres.sorted().map { Browse.Item.genre(item: $0) } }
            progress(result)
        }
    }
}
