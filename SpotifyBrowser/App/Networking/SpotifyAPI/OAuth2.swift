//
//  OAuth2.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 08/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI {
    enum OAuth2 {
        enum CallbackKey: String {
            case code = "code"
            case state = "state"
            case error = "error"
        }

        enum AuthRequestKey: String {
            case redirectUri = "redirect_uri"
            case clientId = "client_id"
            case scope = "scope"
            case responseType = "response_type"
            case showDialog = "show_dialog"
        }

        typealias Scopes = [Scope]

        enum Scope: String {
            case appRemoteControl = "app-remote-control"
            case playlistModifyPrivate = "playlist-modify-private"
            case playlistModifyPublic = "playlist-modify-public"
            case playlistReadCollaborative = "playlist-read-collaborative"
            case playlistReadPrivate = "playlist-read-private"
            case streaming = "streaming"
            case userFollowModify = "user-follow-modify"
            case userFollowRead = "user-follow-read"
            case userLibraryModify = "user-library-modify"
            case userLibraryRead = "user-library-read"
            case userModifyPlayback = "user-modify-playback-state"
            case userReadBirthdate = "user-read-birthdate"
            case userReadCurrentlyPlaying = "user-read-currently-playing"
            case userReadEmail = "user-read-email"
            case userReadPlaybackState = "user-read-playback-state"
            case userReadPrivate = "user-read-private"
            case userReadRecentlyPlayed = "user-read-recently-played"
            case userReadTopRead = "user-top-read"
        }

        struct Token: Codable {
            let token: String
            let expires: Date?
        }
    }
}

extension SpotifyAPI.OAuth2.Token {
    init(token: String, expiresIn: Int?) {
        self.token = token

        if let expiresIn = expiresIn {
            expires = Date().addingTimeInterval(TimeInterval(expiresIn))
        } else {
            expires = nil
        }
    }
}

extension SpotifyAPI.OAuth2.Scopes {
    static var readOnly: SpotifyAPI.OAuth2.Scopes {
        return [.userReadPrivate, .userReadEmail, .userReadTopRead,
                .userFollowRead, .userLibraryRead, .userReadBirthdate,
                .userReadCurrentlyPlaying, .playlistReadPrivate, .playlistReadCollaborative,
                .userReadPlaybackState, .userReadRecentlyPlayed] }
}
