//
//  RequestRetrier.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Alamofire

extension SpotifyAPI {
    final class RequestRetrier: Alamofire.RequestRetrier {
        private let client: HTTPClient
        private let clientId: String
        private let clientSecret: String
        private let storage: StorageService

        init(accountsClient: HTTPClient, clientId: String, clientSecret: String, storage: StorageService) {
            client = accountsClient
            self.clientId = clientId
            self.clientSecret = clientSecret
            self.storage = storage
        }

        func should(_ manager: SessionManager, retry request: Request, with error: Swift.Error, completion: @escaping RequestRetryCompletion) {
            guard (error as? AFError)?.responseCode == 401, request.retryCount < 3 else {
                completion(false, 0)
                return
            }

            guard let refreshToken = storage.refreshToken else {
                Log.error("could not refresh access_token, refresh_token is missing!")
                completion(false, 0)
                return
            }

            Log.info("refreshing access token...")

            let refresh = v1.Endpoint.Token.get(clientId: clientId, clientSecret: clientSecret, grantType: .refreshToken(refreshToken: refreshToken.token))
            client.request(refresh) { [unowned self] result in
                switch result {
                case .success(let response):
                    self.storage.accessToken = OAuth2.Token(token: response.accessToken, expiresIn: response.expiresIn)

                    Log.debug("access token refreshed successfully, retry request: \(request.debugDescription)")
                    completion(true, 0)
                case .failure(let error):
                    Log.error("could not refresh access token: \(error.localizedDescription)")
                    completion(false, 0)
                }
            }
        }
    }

}
