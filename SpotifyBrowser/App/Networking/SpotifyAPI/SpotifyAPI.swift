//
//  SpotifyAPI.swift
//  HTTPClient
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

enum SpotifyAPI {
    static var client: HTTPClient { return HTTPClient(urlString: Config.SpotifyAPI.apiURL) }
    static var accounts: HTTPClient { return HTTPClient(urlString: Config.SpotifyAPI.accountsApiURL) }
}

extension SpotifyAPI {
    private static var jsonDecoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }

    static func endpoint<Response: Decodable>(method: Method = .get, path: Path, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Endpoint<Response> {
        return Endpoint(method: method, path: path, parameters: parameters, headers: headers, decoder: jsonDecoder)
    }
}

extension SpotifyAPI {
    enum v1 {
        enum Endpoint {}
        enum Response {}
    }
}

extension SpotifyAPI.v1.Endpoint {
    typealias Response = SpotifyAPI.v1.Response
}

extension SpotifyAPI {
    final class ErrorMapper: HTTPClientResponseErrorMapper {
        func map(_ data: Data) throws -> Swift.Error {
            let errorResponse: SpotifyAPI.ErrorResponse = try map(data)
            return errorResponse.error
        }
    }
}
