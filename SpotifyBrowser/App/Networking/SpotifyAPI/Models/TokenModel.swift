//
//  Token.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Token: Decodable {
        let accessToken: String
        let tokenType: String
        let scope: String
        let expiresIn: Int
        let refreshToken: String?
    }
}
