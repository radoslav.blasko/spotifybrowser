//
//  Track.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Track: Decodable {
        let id: String
        let name: String
        let trackNumber: Int
        let artists: [Artist]
        let availableMarkets: [String]
        let discNumber: Int
        let duration: Int
        let explicit: Bool
    }
}
