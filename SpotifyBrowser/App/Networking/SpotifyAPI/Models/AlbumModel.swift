//
//  Album.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Album: Decodable {
        let albumType: String
        let artists: [Artist]
        let availableMarkets: [String]
        let copyrights: [Copyright]
        let genres: [String]
        let href: URL
        let id: String
        let images: [Image]
        let label: String
        let name: String
        let popularity: Int
        let releaseDate: String
        let releaseDatePrecision: String
        let tracks: [Track]
    }

    struct AlbumItem: Decodable {
        let addedAt: Date
        let album: Album
    }
}
