//
//  CopyrightModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Copyright: Decodable {
        let text: String
        let type: Type
    }
}

extension SpotifyAPI.v1.Response.Copyright {
    enum `Type`: String, Decodable {
        case copyright = "C"
        case soundRecordingCopyright = "P"
    }
}
