//
//  SavedTrackModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct SavedTrack: Decodable {
        let addedAt: Date
        let track: Track
    }
}
