//
//  PlaylistModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Playlist: Decodable {
        let id: String
        let name: String
        let collaborative: Bool
        let images: [Image]
        let owner: User
        let `public`: Bool
        let snapshotId: String
    }

    struct Playlists: Decodable {
        let playlists: Paging<Playlist>
    }
}
