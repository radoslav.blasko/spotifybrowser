//
//  PagingModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Paging<Item: Decodable>: Decodable {
        let href: String
        let items: [Item]
        let limit: Int
        let offset: Int
        let total: Int
        let next: String?
        let previous: String?
    }
}
