//
//  User.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct User: Decodable {
        let id: String
        let email: String
        let product: String
        let images: [Image]
        let displayName: String?
        let birthdate: DecodableDate<SpotifyAPI.DateFormatter.YYYYmmdd>?
        let country: String?
    }
}
