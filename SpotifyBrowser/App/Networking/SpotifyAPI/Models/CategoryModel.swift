//
//  CategoryModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    struct Category: Decodable {
        let id: String
        let name: String
        let icons: [Image]
        let href: String
    }

    struct Categories: Decodable {
        let categories: Paging<Category>
    }
}

extension SpotifyAPI.v1.Response.Category: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
