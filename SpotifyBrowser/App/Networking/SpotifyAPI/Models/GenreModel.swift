//
//  GenreModel.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 13/01/2020.
//  Copyright © 2020 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Response {
    typealias Genre = String

    struct Genres: Decodable {
        let genres: [Genre]
    }
}
