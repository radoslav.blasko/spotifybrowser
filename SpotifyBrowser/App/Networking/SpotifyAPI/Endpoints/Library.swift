//
//  Library.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Endpoint {
    struct Library {
        static func myAlbums(paging: SpotifyAPI.PagingParam? = nil) -> Endpoint<Response.Paging<Response.AlbumItem>> {
            return SpotifyAPI.endpoint(path: "me/albums", parameters: paging?.params)
        }

        static func savedTracks(paging: SpotifyAPI.PagingParam? = nil) -> Endpoint<Response.Paging<Response.SavedTrack>> {
            return SpotifyAPI.endpoint(path: "me/tracks", parameters: paging?.params)
        }
    }
}
