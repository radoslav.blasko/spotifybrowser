//
//  Browse.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Endpoint {
    struct Browse {
        static func categories(paging: SpotifyAPI.PagingParam? = nil) -> Endpoint<Response.Categories> {
            return SpotifyAPI.endpoint(path: "browse/categories", parameters: paging?.params)
        }

        static func category(id: String) -> Endpoint<Response.Category> {
            return SpotifyAPI.endpoint(path: "browse/categories/\(id)")
        }

        static func categoryPlaylists(id: String, paging: SpotifyAPI.PagingParam? = nil) -> Endpoint<Response.Playlists> {
            return SpotifyAPI.endpoint(path: "browse/categories/\(id)/playlists", parameters: paging?.params)
        }

        static func recommendationGenres(paging: SpotifyAPI.PagingParam? = nil) -> Endpoint<Response.Genres> {
            return SpotifyAPI.endpoint(path: "recommendations/available-genre-seeds", parameters: paging?.params)
        }
    }
}
