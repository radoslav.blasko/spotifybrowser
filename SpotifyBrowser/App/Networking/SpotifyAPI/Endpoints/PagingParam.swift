//
//  PagingParam.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI {
    struct PagingParam {
        let limit: Int
        let offset: Int = 0

        var params: Parameters {
            return ["limit": limit, "offset": offset]
        }
    }
}
