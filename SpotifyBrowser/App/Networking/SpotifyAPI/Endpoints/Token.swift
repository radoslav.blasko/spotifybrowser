//
//  Token.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 23/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Endpoint {
    struct Token {
        enum GrantType {
            case authorizationCode(code: String, redirectUri: String)
            case refreshToken(refreshToken: String)

            var paramValue: String {
                switch self {
                case .authorizationCode:
                    return "authorization_code"
                case .refreshToken:
                    return "refresh_token"
                }
            }
        }

        static func get(clientId: String, clientSecret: String, grantType: GrantType) -> Endpoint<Response.Token> {
            var params: Parameters = ["grant_type": grantType.paramValue]

            switch grantType {
            case .authorizationCode(let code, let redirectUri):
                params["code"] = code
                params["redirect_uri"] = redirectUri
            case .refreshToken(let refreshToken):
                params["refresh_token"] = refreshToken
            }

            let auth = authHeader(clientId: clientId, clientSecret: clientSecret)
            return SpotifyAPI.endpoint(method: .post, path: "token", parameters: params, headers: ["Authorization": auth])
        }

        static func authHeader(clientId: String, clientSecret: String) -> String {
            let secretAndId = "\(clientId):\(clientSecret)".data(using: .utf8)?.base64EncodedString() ?? ""
            return "Basic " + secretAndId
        }
    }
}
