//
//  User.swift
//  spotifybrowser
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI.v1.Endpoint {
    struct User {
        static func get(id: String) -> Endpoint<Response.User> {
            return SpotifyAPI.endpoint(path: "users/\(id)")
        }

        static var me: Endpoint<Response.User> {
            return SpotifyAPI.endpoint(path: "me")
        }
    }
}
