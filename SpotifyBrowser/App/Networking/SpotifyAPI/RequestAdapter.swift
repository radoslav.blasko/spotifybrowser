//
//  RequestAdapter.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation
import Alamofire

extension SpotifyAPI {
    final class RequestAdapter: Alamofire.RequestAdapter {
        private let accessToken: (() -> SpotifyAPI.OAuth2.Token?)

        init(accessToken: @escaping (() -> SpotifyAPI.OAuth2.Token?)) {
            self.accessToken = accessToken
        }

        func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            guard let accessToken = accessToken() else {
                Log.error("missing access token! Could not authorize request: \(urlRequest.debugDescription)")
                return urlRequest
            }

            var request = urlRequest
            var headers = request.allHTTPHeaderFields ?? [:]
            headers["Authorization"] = "Bearer " + accessToken.token
            request.allHTTPHeaderFields = headers

            return request
        }
    }
}

