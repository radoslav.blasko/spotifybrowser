//
//  DateFormatterYYYYmmdd.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

extension SpotifyAPI {
    enum DateFormatter {}
}

extension SpotifyAPI.DateFormatter {
    final class YYYYmmdd: CachedDateFormatter {
        static var cached: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-mm-dd"
            return formatter
        }
    }
}

