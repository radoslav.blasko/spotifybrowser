//
//  HTTPClient.swift
//  HTTPClient
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation
import Alamofire

class HTTPClient {
    let baseURL: URL
    let sessionManager: SessionManager
    var errorMapper: HTTPClientResponseErrorMapper? = nil

    private let serializationQueue = DispatchQueue(label: "com.httpclient.serialization.queue")

    init(url: URL, errorMapper: HTTPClientResponseErrorMapper?) {
        baseURL = url
        sessionManager = SessionManager()
        self.errorMapper = errorMapper
    }

    convenience init(urlString: String, errorMapper: HTTPClientResponseErrorMapper? = nil) {
        guard let url = URL(string: urlString) else {
            fatalError("Invalid URL: \(urlString)!")
        }
        self.init(url: url, errorMapper: errorMapper)
    }

    @discardableResult
    func request<Response>(_ endpoint: Endpoint<Response>, resume: Bool = true, completion: ((Result<Response>) -> Void)?) -> Request {
        let request = sessionManager.request(url(endpoint.path), method: endpoint.method, parameters: endpoint.parameters, headers: endpoint.headers)
        request.validate().responseData(queue: serializationQueue) { response in
            Log.debug("did finish loading request: [\(response.response?.statusCode ?? -999)] \(response.request?.url?.absoluteString ?? "") [\(String(format: "%.3fs", response.timeline.totalDuration))]")

            var result = response.result.flatMap(endpoint.decode)

            if result.isFailure, let data = response.data, let mappedError = try? self.errorMapper?.map(data) {
                result = .failure(mappedError)
            }

            Environment.current.dispatch(ifNot: .production, execute: {
                guard result.isFailure else { return }
                Log.error("failed to receive data for request: \(request.debugDescription), error: \(String(describing: result.error))")
            })

            Log.verbose("response data: \(String(data: response.data ?? Data(), encoding: .utf8) ?? "")")

            DispatchQueue.main.async { completion?(result) }
        }

        if resume {
            request.resume()
        }

        return request
    }

    private func url(_ path: Path) -> URL {
        return baseURL.appendingPathComponent(path)
    }
}

protocol HTTPClientResponseErrorMapper {
    func map(_ data: Data) throws -> Error
}

extension HTTPClientResponseErrorMapper {
    func map<E: Error>(_ data: Data) throws -> E where E: Decodable {
        return try JSONDecoder().decode(E.self, from: data)
    }
}
