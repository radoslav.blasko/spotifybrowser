//
//  Endpoint.swift
//  HTTPClient
//
//  Created by Radoslav Blasko on 07/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation
import Alamofire

typealias Path = String
typealias Parameters = [String: Any]
typealias Method = Alamofire.HTTPMethod
typealias HTTPHeaders = Alamofire.HTTPHeaders

struct Endpoint<Response> {
    let path: Path
    let method: Method
    let parameters: Parameters?
    let headers: HTTPHeaders?
    let decode: (Data) throws -> Response
}

extension Endpoint where Response: Decodable {
    init(method: Method = .get, path: Path, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, decoder: JSONDecoder = JSONDecoder()) {
        self.init(path: path, method: method, parameters: parameters, headers: headers) {
            try decoder.decode(Response.self, from: $0)
        }
    }
}

extension Endpoint where Response == Void {
    init(method: Method = .get, path: Path, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) {
        self.init(path: path, method: method, parameters: parameters, headers: headers, decode: { _ in })
    }
}

extension Endpoint where Response == String {
    init(method: Method = .get, path: Path, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) {
        self.init(path: path, method: method, parameters: parameters, headers: headers, decode: {
            guard let decoded = String(data: $0, encoding: .utf8) else {
                throw AFError.responseSerializationFailed(reason: .stringSerializationFailed(encoding: .utf8))
            }
            return decoded
        })
    }
}
