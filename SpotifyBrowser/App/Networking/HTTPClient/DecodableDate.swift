//
//  DecodableDate.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 30/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

protocol CachedDateFormatter {
    static var cached: DateFormatter { get }
}

struct DecodableDate<Formatter: CachedDateFormatter>: Decodable {
    let date: Date

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let dateString = try container.decode(String.self)
        let formatter = Formatter.cached

        if let date = formatter.date(from: dateString) {
            self.date = date
        } else {
            let debugDescription = "Date string (\(dateString)) does not match format expected by formatter (\(String(describing: formatter.dateFormat)))."
            throw DecodingError.dataCorruptedError(in: container, debugDescription: debugDescription)
        }
    }
}
