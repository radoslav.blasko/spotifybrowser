//
//  URLScheme.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 09/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension AppURL {
    enum URLScheme: String {
        case base = "spotifybrowser"
    }
}
