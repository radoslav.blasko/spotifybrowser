//
//  AppURL.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 09/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

struct AppURL {
    typealias Parameters = [String: String]

    let scheme: URLScheme
    let type: URLType
    let parameters: Parameters?

    init(scheme: URLScheme, type: URLType, parameters: Parameters? = nil) {
        self.scheme = scheme
        self.type = type
        self.parameters = parameters
    }

    init?(url: URL) {
        let rawType = (url.host ?? "") + url.path
        guard let type = URLType(rawValue: rawType), let scheme = URLScheme(rawValue: url.scheme ?? "") else { return nil }
        self.init(scheme: scheme, type: type, parameters: url.parameters)
    }
}

extension AppURL {
    var url: URL {
        var components = URLComponents()
        components.host = type.host
        components.path = type.path
        components.scheme = scheme.rawValue
        components.queryItems = parameters?.queryItems

        guard let url = components.url else {
            fatalError("Unable to create URL from: \(self)")
        }

        return url
    }
}
