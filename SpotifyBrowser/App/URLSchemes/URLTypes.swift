//
//  URLTypes.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 09/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

extension AppURL {
    enum URLType: String {
        case oauthCallback = "oauth/callback"
    }
}

extension AppURL.URLType {
    var host: String {
        guard let host = rawValue.split(separator: "/").first else {
            fatalError("Could not return host from URLType: \(self)")
        }
        return String(host)
    }

    var path: String {
        var components = rawValue.split(separator: "/")
        components.removeFirst()
        return "/" + components.joined(separator: "/")
    }
}
