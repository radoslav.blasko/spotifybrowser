//
//  Environment.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 10/10/2018.
//  Copyright © 2018 Radoslav Blasko. All rights reserved.
//

import Foundation

enum Environment {
    case develop
    case staging
    case production

    static var current: Environment {
        #if DEVELOP
            return .develop
        #elseif STAGING
            return .staging
        #elseif PRODUCTION
            return .production
        #else
            fatalError("Unknown environment, check the Active compilation flags!")
        #endif
    }
}

extension Environment {
    func dispatch(if env: Environment, execute: () -> Void) {
        guard self == env else { return }
        execute()
    }

    func dispatch(ifNot env: Environment, execute: () -> Void) {
        guard self != env else { return }
        execute()
    }
}
