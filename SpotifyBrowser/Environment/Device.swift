//
//  Device.swift
//  SpotifyBrowser
//
//  Created by Radoslav Blasko on 29/05/2019.
//  Copyright © 2019 Radoslav Blasko. All rights reserved.
//

import Foundation

final class DeviceInfo {
    var identifier: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let mirror = Mirror(reflecting: systemInfo.machine)

        let identifier = mirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }()

    var isSimulator: Bool

    init() {
        isSimulator = ["i386", "x86_64"].contains(identifier)
    }
}

extension DeviceInfo {
    static var current: DeviceInfo { return DeviceInfo() }
}
